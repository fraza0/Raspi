#!/usr/local/bin/python

import RPi.GPIO as GPIO
import time
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(18,GPIO.OUT)

print "LED on"
GPIO.output(18,GPIO.HIGH)
time.sleep(0.25)
print "LED off"
GPIO.output(18,GPIO.LOW)