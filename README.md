# Projects using Raspberry pi, breadboard and electrical compounds


# Motors
https://business.tutsplus.com/tutorials/controlling-dc-motors-using-python-with-a-raspberry-pi--cms-20051


======


# RetroPie

https://github.com/RetroPie/RetroPie-Setup/wiki/First-Installation#transferring-roms

======

# Connecting LED to Raspberry Pi 2B

## Projects

<https://thepihut.com/blogs/raspberry-pi-tutorials/27968772-turning-on-an-led-with-your-raspberry-pis-gpio-pins>

<https://projects.drogon.net/raspberry-pi/gpio-examples/tux-crossing/gpio-examples-1-a-single-led/>

## WiringPi

<http://wiringpi.com/pins/>
    
### Raspberry Pi pinout
    $ gpio readall


======


# Host Apache2 WebServer

https://www.raspberrypi.org/documentation/remote-access/web-server/apache.md

http://raspberrypg.org/2013/11/step-5-installing-postgresql-on-my-raspberry-pi/

# Git Server

https://www.sitepoint.com/setting-up-your-raspberry-pi-as-a-git-server/

#### Change /home/git to /mnt/usbdrive/ (...)
http://askubuntu.com/questions/21321/move-home-folder-to-second-drive

# Push Button

https://en.wikipedia.org/wiki/Pull-up_resistor
http://makezine.com/projects/tutorial-raspberry-pi-gpio-pins-and-python/
http://razzpisampler.oreilly.com/ch07.html

# FTP Server

https://www.raspberrypi.org/documentation/remote-access/ftp.md
    https://www.pureftpd.org/project/pure-ftpd
    https://pt.wikipedia.org/wiki/Transport_Layer_Security
https://raspguide.wordpress.com/2014/03/22/how-to-make-a-secure-ftp-server-with-vsftpd-part-1/


======


# Gitlab Server - Raspberry Pi


## Markdown Cheatsheet
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

## Installing Omnibus-Gitlab
https://about.gitlab.com/downloads/#raspberrypi2
    
### NGINX tweaks
https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/settings/nginx.md
    
### Store git data in alternative directory:
https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/settings/configuration.md#storing-git-data-in-an-alternative-directory
    
## Gitlab documentation
https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab
    
## Troubleshooting Guide: 
https://github.com/gitlabhq/gitlab-public-wiki/wiki/Trouble-Shooting-Guide
    
###### gitlab.rb Template
https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template
